Variable	Default ([Range])	Type	Comment
core.wd	(not set)	directory	current working directory
core.extension	.fit	string	FITS file extension
core.force_16bit	false	boolean	don't use 32 bits for pixel depth
core.allow_heterogeneous_fitseq	false	boolean	allow FITS cubes to have different sizes
core.mem_mode	0 [0, 1]	integer	memory mode (0 ratio, 1 amount)
core.mem_ratio	0.9 [0.05, 4]	double	memory ratio of available
core.mem_amount	10 [0.1, 1e+06]	double	amount of memory in GB
core.hd_bitdepth	20 [17, 24]	integer	HD AutoStretch bit depth
core.script_check_requires	true	boolean	need requires cmd in script
core.pipe_check_requires	false	boolean	need requires cmd in pipe
core.check_updates	true	boolean	check update at start-up
core.lang	(not set)	string	active siril language
core.swap_dir	os dependant	directory	swap directory
core.wcs_formalism	1 [0, 1]	integer	WCS formalism used in FITS header
core.catalogue_namedstars	:ref:`(*) <kstars>`	string	Path of the namedstars.dat catalogue
core.catalogue_unnamedstars	:ref:`(*) <kstars>`	string	Path of the unnamedstars.dat catalogue
core.catalogue_tycho2	:ref:`(*) <kstars>`	string	Path of the deepstars.dat catalogue
core.catalogue_nomad	:ref:`(*) <kstars>`	string	Path of the USNO-NOMAD-1e8.dat catalogue
core.rgb_aladin	false	boolean	add CTYPE3='RGB' in the FITS header
core.copyright	(not set)	string	user copyright to put in file header
core.starnet_exe	(not set)	string	location of the StarNet executable
core.starnet_weights	(not set)	string	location of the StarNet-torch weights file
core.gnuplot_dir	(not set)	string	directory of the gnuplot installation
core.asnet_dir	(not set)	string	directory of the asnet_ansvr installation
core.fftw_timelimit	60	double	FFTW planning timelimit
core.fftw_conv_fft_cutoff	15	integer	Convolution minimum kernel size to use FFTW
core.fftw_strategy	1	integer	FFTW planning strategy
core.fftw_timeout	true	boolean	multithreaded FFTW
starfinder.focal_length	0 [0, 999999]	double	focal length in mm for radius adjustment
starfinder.pixel_size	0 [0, 99]	double	pixel size in µm for radius adjustment
debayer.use_bayer_header	true	boolean	use pattern from the file header
debayer.pattern	0 [0, 7]	integer	index of the Bayer pattern
debayer.interpolation	8 [0, 10]	integer	type of interpolation
debayer.top_down	true	boolean	force debayer top-down
debayer.offset_x	0 [0, 1]	integer	Bayer matrix offset X
debayer.offset_y	0 [0, 1]	integer	Bayer matrix offset Y
debayer.xtrans_passes	1 [1, 4]	integer	Number of passes for the X-Trans Markesteijn algorithm
photometry.gain	2.3 [0, 10]	double	electrons per ADU for noise estimation
photometry.inner	20 [2, 100]	double	inner radius for background annulus
photometry.outer	30 [3, 200]	double	outer radius for background annulus
photometry.inner_factor	4.2 [2, 50]	double	factor for inner radius automatic computation
photometry.outer_factor	6.3 [2, 50]	double	factor for outer radius automatic computation
photometry.force_radius	false	boolean	force flux aperture value
photometry.aperture	10 [1, 100]	double	forced aperture for flux computation
photometry.minval	-1500 [-65536, 65534]	double	minimum valid pixel value for photometry
photometry.maxval	60000 [1, 65535]	double	maximum valid pixel value for photometry
astrometry.asnet_percent_scale_range	20 [0, 10000]	integer	percent below and above the expected sampling to allow
astrometry.asnet_sip_order	0 [0, 6]	integer	degrees of the polynomial correction
astrometry.asnet_radius	10 [0.01, 180]	double	radius around the target coordinates (degrees)
astrometry.asnet_keep_xyls	false	boolean	do not delete .xyls FITS tables
astrometry.asnet_keep_wcs	false	boolean	do not delete .wcs result files
astrometry.asnet_max_seconds_run	10 [0, 100000]	integer	maximum seconds to try solving
astrometry.asnet_show_output	false	boolean	show solve-field output in main log
astrometry.update_default_scale	true	boolean	update default focal length and pixel size from the result
analysis.panel	256 [127, 1024]	integer	panel size of aberration inspector
analysis.window	381 [300, 1600]	integer	window size of aberration inspector
compression.enabled	false	boolean	FITS compression enabled
compression.method	0 [0, 3]	integer	FITS compression method
compression.quantization	16 [8, 256]	double	quantization factor for 32-bit float
compression.hcompress_scale	4 [0, 256]	double	Hcompress scale factor
gui_prepro.cfa	false	boolean	type of sensor for cosmetic correction
gui_prepro.equalize_cfa	true	boolean	equalize flat channels
gui_prepro.fix_xtrans	false	boolean	enable correction for X-Trans sensor
gui_prepro.xtrans_af_x	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_af_y	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_af_w	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_af_h	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_sample_x	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_sample_y	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_sample_w	0	integer	if no X-Trans model found, use this
gui_prepro.xtrans_sample_h	0	integer	if no X-Trans model found, use this
gui_prepro.bias_lib	(not set)	string	default master bias
gui_prepro.use_bias_lib	false	boolean	use default master bias
gui_prepro.dark_lib	(not set)	string	default master dark
gui_prepro.use_dark_lib	false	boolean	use default master dark
gui_prepro.flat_lib	(not set)	string	default master flat
gui_prepro.use_flat_lib	false	boolean	use default master flat
gui_prepro.stack_default	$seqname$stacked	string	default stack name
gui_prepro.use_stack_default	true	boolean	use preferred stack name
gui_registration.method	0 [0, 7]	integer	index of the selected registration method
gui_registration.interpolation	4 [0, 5]	integer	index of the selected interpolation method
gui_registration.clamping	true	boolean	use clamping method with Lanczos and Cubic interpolation
gui_stack.method	0 [0, 4]	integer	index of the selected method
gui_stack.normalization	3 [0, 4]	integer	index of the normalization method
gui_stack.rejection	5 [0, 7]	integer	index of the rejection method
gui_stack.sigma_low	3 [0, 20]	double	sigma low value for rejection
gui_stack.sigma_high	3 [0, 20]	double	sigma high value for rejection
gui_stack.linear_low	5 [0, 20]	double	linear low value for rejection
gui_stack.linear_high	5 [0, 20]	double	linear high value for rejection
gui_stack.percentile_low	3 [0, 100]	double	percentile low value for rejection
gui_stack.percentile_high	3 [0, 100]	double	percentile high value for rejection
gui.first_start	true	boolean	first start of siril
gui.silent_quit	false	boolean	don't confirm quit when exiting
gui.silent_linear	false	boolean	don't confirm save when non linear mode
gui.remember_windows	true	boolean	remember window position
gui.main_win_pos_x	0	integer	main window position
gui.main_win_pos_y	0	integer	main window position
gui.main_win_pos_w	0	integer	main window position
gui.main_win_pos_h	0	integer	main window position
gui.pan_position	-1	integer	position of the two sides separator
gui.extended	true	boolean	main window is extended
gui.maximized	false	boolean	main window is maximized
gui.theme	0 [0, 1]	integer	index of the selected theme
gui.font_scale	100	double	font scale in percent
gui.icon_symbolic	false	boolean	icon style
gui.script_path		list of strings	list of script directories
gui.warn_script_run	true	boolean	warn when launching a script
gui.show_thumbnails	true	boolean	show thumbnails in open dialog
gui.thumbnail_size	256	integer	size of the thumbnails
gui.selection_guides	0	integer	number of elements of the grid guides
gui.show_deciasec	false	boolean	show tenths of arcseconds on hover
gui.default_rendering_mode	0 [0, 6]	integer	default display mode
gui.display_histogram_mode	0 [0, 1]	integer	default histogram display mode
gui_astrometry.compass_position	1 [0, 5]	integer	index of the compass position over grid
gui_astrometry.cat_messier	true	boolean	show Messier objects in annotations
gui_astrometry.cat_ngc	true	boolean	show NGC objects in annotations
gui_astrometry.cat_ic	true	boolean	show IC objects in annotations
gui_astrometry.cat_ldn	true	boolean	show LDN objects in annotations
gui_astrometry.cat_sh2	true	boolean	show SH2 objects in annotations
gui_astrometry.cat_stars	true	boolean	show stars in annotations
gui_astrometry.cat_user	true	boolean	show user objects in annotations
gui_pixelmath.pm_presets		list of strings	list of pixel math presets