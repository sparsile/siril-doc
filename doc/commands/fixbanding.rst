| Tries to remove the canon banding.
| 
| Argument **amount** defines the amount of correction.
| **Sigma** defines a protection level of the algorithm, higher sigma gives higher protection.
| **-vertical** option enables to perform vertical banding removal
