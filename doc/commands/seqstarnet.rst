| This command calls `Starnet++ <https://www.starnetastro.com/>`__ to remove stars from the current sequence. The first argument must be the sequence from which to remove stars: all the other available arguments are the same as for the STARNET command
| 
| Links: :ref:`starnet <starnet>`
