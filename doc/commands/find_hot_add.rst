|
| Lines ``P x y type`` will fix the pixel at coordinates (x, y) type is an optional character (C or H) specifying to Siril if the current pixel is cold or hot. This line is created by the command FIND_HOT but you also can add some lines manually:
| Lines ``C x 0 type`` will fix the bad column at coordinates x.
| Lines ``L y 0 type`` will fix the bad line at coordinates y.
