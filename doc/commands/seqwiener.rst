| The same as the **WIENER** command, but applies to a sequence which must be specified as the first argument
| 
| Links: :ref:`wiener <wiener>`
