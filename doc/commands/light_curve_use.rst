.. code-block:: text

    light_curve sequencename channel [-autoring] { -at=x,y | -wcs=ra,dec } { -refat=x,y | -refwcs=ra,dec } ...
    light_curve sequencename channel [-autoring] -ninastars=file