| Reset the magnitude calibration to 0. See SETMAG
| 
| Links: :ref:`setmag <setmag>`
