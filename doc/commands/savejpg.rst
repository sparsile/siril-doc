| Saves current image into a JPG file: **filename**.jpg.
| 
| You have the possibility to adjust the quality of the compression. A value 100 for **quality** parameter offers best fidelity while a low value increases the compression ratio. If no value is specified, a default value of 100 is applied
