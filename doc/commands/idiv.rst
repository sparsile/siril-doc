| Divides the image in memory by the image **filename** given in argument.
| Result will be in 32 bits per channel if allowed in the preferences. See also FDIV
| 
| Links: :ref:`fdiv <fdiv>`
