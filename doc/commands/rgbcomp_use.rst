.. code-block:: text

    rgbcomp red green blue [-out=result_filename]
    rgbcomp -lum=image { rgb_image | red green blue } [-out=result_filename]