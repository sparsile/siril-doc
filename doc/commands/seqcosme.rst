| Same command as COSME but for the the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "cosme\_" unless otherwise specified with option **-prefix=**
| 
| Links: :ref:`cosme <cosme>`
