.. code-block:: text

    makepsf clear
    makepsf load filename
    makepsf save [filename]
    makepsf blind [-l0] [-si] [-multiscale] [-lambda=] [-comp=] [-ks=] [-savepsf=]
    makepsf stars [-sym] [-ks=] [-savepsf=]
    makepsf manual { -gaussian | -moffat | -disc | -airy } [-fwhm=] [-angle=] [-ratio=] [-beta=] [-dia=] [-fl=] [-wl=] [-pixelsize=] [-obstruct=] [-ks=] [-savepsf=]