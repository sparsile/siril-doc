| Restores an image using the Wiener deconvolution method.
| 
| Optionally, a PSF may be loaded using the argument **-loadpsf=\ filename**.
| 
| The parameter **-alpha=** provides the Gaussian noise modelled regularization factor
| 
| Links: :ref:`psf <psf>`
