#########################
Installation on GNU/Linux
#########################

.. toctree::
   :hidden:

Installation of the AppImage binary
===================================

For GNU/Linux systems, we decided to provide a universal binary that works on 
GNU/Linux-like systems (x86_64). To run the AppImage binary, you just have to 
download it and allow it to run using the command:

.. code-block:: bash
      
      chmod +x Path/To/Application/Siril-x.y.z-x86_64.AppImage

By replacing with the correct path and x,y and z with the version numbers. Then
a simple double-click on the AppImage starts Siril.

Installation of the flatpak
===========================

Another way to install sable version of siril is to use the 
`flatpak <https://flathub.org/apps/details/org.free_astro.siril>`_, the utility
for software deployment and package management for Linux.
To install flatpak, type the following command:

.. code-block:: bash
      
      flatpak install flathub org.free_astro.siril

Then, to run the application:

.. code-block:: bash
      
      flatpak run org.free_astro.siril


Installation from sources
=========================

Installation from source is required if you want the latest features, if the 
past release is getting old, if you want to participate in improving Siril or 
not use all the dependencies.

Getting the sources
~~~~~~~~~~~~~~~~~~~

The sources are stored on a git repository, which you can download with this 
command the first time:

.. code-block:: bash
      
      git clone https://gitlab.com/free-astro/siril.git
      cd siril
      git submodule update --init
   
And update it the following times by typing ``git pull`` in the base siril 
directory.

Dependencies
~~~~~~~~~~~~

Siril depends on a number of libraries, most of which should be available in 
your operating system if it is recent enough. The names of the packages 
specific to operating systems are listed in each section below. Mandatory 
dependencies are:

* `gtk+3 <http://www.gtk.org/>`_ (Graphical user interface library), at least 
  version 3.20
* `cfitsio <http://heasarc.nasa.gov/fitsio/fitsio.html>`_ (FITS images support)
* `fftw <http://www.fftw.org/>`_ (Discrete Fourier Transform library)
* `gsl <http://www.gnu.org/software/gsl/>`_ (The GNU Scientific Library), 
  version 1 or 2 starting with release 0.9.1 or SVN revision 1040
* `OpenCV <http://opencv.org/>`_ and a C++ compiler for some image operations
* `Exiv2 <https://www.exiv2.org/>`_ to manage image metadata.
* `libcurl <https://curl.haxx.se/libcurl/>`_ OR 
  `glib-networking <https://gitlab.gnome.org/GNOME/glib-networking>`_ with its 
  HTTP backend for online operations like update checks, astrometry and 
  photometry requests.

.. note::
   Even if Siril can run in console since version 0.9.9, it is still linked 
   against the graphical libraries, so you still need GTK+ to compile and run 
   it.

Optional dependencies are:

* `libraw <http://www.libraw.org/>`_, `libtiff <http://www.libtiff.org/>`_, 
  `libjpeg <http://libjpeg.sourceforge.net/>`_, 
  `libpng <http://libpng.sourceforge.net/index.html>`_, 
  `libheif <https://github.com/strukturag/libheif>`_ for RAW, TIFF, JPEG, PNG 
  and HEIF images import and export. The libraries are detected at 
  compilation-time.
* `FFMS2 <https://github.com/FFMS/ffms2>`_ for film native support as image 
  sequences. It also allows frames to be extracted from many kinds of film, for
  other purposes than astronomy. Versions < 2.20 have an annoying bug. It is 
  recommended to install the latest version.
* `ffmpeg <https://www.ffmpeg.org/>`_ (or libav), providing libavformat, 
  libavutil (>= 55.20), libavcodec, libswscale and libswresample for mp4 
  sequence export
* `gnuplot <http://www.gnuplot.info/>`_ for photometry graph creation (not 
  required at compilation time)
* `wcslib <https://www.gnu.org/software/gnuastro/manual/html_node/WCSLIB.html>`_ 
  for world coordinate system management, annotations and the photometric color
  calibration
* `libconfig <https://hyperrealm.github.io/libconfig/>`_ (Structured 
  configuration files support), used to read the configuration file from 
  versions up to 1.0, only used to get old settings now
* `libjson-glib <https://gitlab.gnome.org/GNOME/json-glib>`_ for update 
  checking (useless if you build an non-released version).

Build dependencies
To install from source, you will have to install the base development packages::

   git, autoconf, automake, libtool, intltool, pkg-tools, make, cmake, gcc, g++

The compilers gcc and g++ from this list can be replaced by clang and clang++ 
(we use them for development), probably others as well.

The autotools packages (autoconf, automake, probably some others) can be 
replaced by meson.

Generic build process
~~~~~~~~~~~~~~~~~~~~~

Siril can be compiled either using autotools or meson.

Meson
"""""

The recommended way is to use meson and ninja:

.. code-block:: bash
      
   meson --buildtype release _build
   cd _build
   ninja
   ninja install

To disable some dependencies or features, use meson options ``-Dfeature=false``
or ``-Ddisable-feature=yes`` depending on the case.

Autotools
"""""""""

The autotools ways is well known in the unix world, once the source has been 
downloaded and the prerequisites have been installed, the general way to build 
it is as such:

.. code-block:: bash

   ./autogen.sh
   make
   make install

possibly with superuser privileges for the last line.

You may want to pass specific options to the compiler, for example like that if
you want optimisation and installation in ``/opt`` instead of the default 
``/usr/local``:

.. code-block:: bash

   CFLAGS='-mtune=native -O3' ./autogen.sh --prefix=/opt
   
To launch Siril, the command name is :program:`siril` or :program:`siril-cli`.

Installation on Debian-like systems
-----------------------------------

You may want to build a ``.deb`` package instead of using a non-packaged 
version, in that case see this `help <https://wiki.debian.org/BuildingTutorial>`_. 
In particular, to install dependencies, you can use the command:

.. code-block:: bash

   apt build-dep siril
   
Otherwise, here is the list of packages for the current version:

* Packages required for the build system:

.. code-block:: bash

   autoconf automake make gcc g++ libtool intltool pkg-config cmake
   
* List of packages for mandatory dependencies:

.. code-block:: bash

   libfftw3-dev libgsl-dev libcfitsio-dev libgtk-3-dev libopencv-dev
   libexiv2-dev
   
* List of packages for optional dependencies:

.. code-block:: bash
   
   wcslib-dev libcurl4-gnutls-dev libpng-dev libjpeg-dev libtiff5-dev 
   libraw-dev gnome-icon-theme libavformat-dev libavutil-dev libavcodec-dev 
   libswscale-dev libswresample-dev

for film input (AVI and others) support: 

.. code-block:: bash
  
   libffms2-dev

Installation on Arch Linux
--------------------------
Two packages are available on AUR: :program:`siril` and ``siril-git``. Download the 
``PKGBUILD`` or the repository, install dependencies, run makepkg to build the 
package and ``pacman -U`` to install it.

Dependencies (mandatory and a few optional):

.. code-block:: bash

      pacman -S base-devel cmake git intltool gtk3 fftw cfitsio gsl opencv 
      exiv2 libraw wcslib
