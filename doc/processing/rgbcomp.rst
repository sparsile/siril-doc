RGB compositing
===============

.. figure:: ../_images/processing/rgb_comp_dialog.png
   :alt: RGB composition dialog
   :class: with-shadow
   
The RGB composition tool allows you to assemble up to 8 monochrome images to 
form a single color image. The images can be shifted by translation but not by 
rotation, otherwise it will not be possible to register them. In such a situation, 
it is necessary to create a mini sequence of the input images and register them 
with the global registration algorithm.

The operation of this tool is quite simple, just load the images and assign 
them a color. The first field, optional, is reserved for the luminance layer.
Once a luminance layer is loaded you can integrate it or not in the composition
thans to the :guilabel:`Use Luminance` button. Each color can be customized by 
clicking on it and choosing a new one. When more than 3 images (or 4 if there 
is luminance) are loaded, it may be necessary to adjust the brightness of each 
channel. The :guilabel:`Adjust layers brightness` button performs this 
operation automatically.

.. note::
   For binning and image dimensions, the first loaded image determines the size
   of the output image. If you have images of different sizes, you should 
   always load the largest first. If your images are different just because of 
   bining, so with the same field of view, the composition tool will upscale 
   the smaller images when they are loaded to match the size of the first 
   loaded image. It is useful for the common L-RGB taken with the colour 
   filters in bin 2. This also means that if two images have not been taken 
   with the same sensor, it is unlikely they will have the same field of view 
   and pixel sampling after image resampling, and this will not work with this 
   tool.

Three color spaces are available for rendering the composition:

* `HSL <https://en.wikipedia.org/wiki/HSL_and_HSV>`_ (for hue, saturation, 
  lightness)
* `HSV <https://en.wikipedia.org/wiki/HSL_and_HSV>`_ (for hue, saturation, 
  value; also known as HSB, for hue, saturation, brightness) 
* `CIE L*a*b* <https://en.wikipedia.org/wiki/CIELAB_color_space>`_

and are left to the choice of the user.

Once the composition is finished, it is possible to do the color balance by 
clicking on the button :guilabel:`Finalize color balance`: this opens the 
:ref:`color calibration dialog <processing/colors:Manual Color Calibration>`.

If the images are not aligned with each other, and they are just shifted by 
translation, then it is possible to align them. Two algorithms are possible:

* **One star registration (deep-sky)**: you have to draw a selection around a 
  star, making sure that the selection contains the star in all channels.
* **Image pattern alignment (planetary/deepsky)**: you have to draw a selection 
  around the object you want to align. A high enough contrast is required for 
  the algorithm to work properly.

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/rgbcomp_use.rst

   .. include:: ../commands/rgbcomp.rst
