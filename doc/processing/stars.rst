Star Processing
############### 

Stars are an integral part of deep sky images and play a crucial role in 
bringing out the beauty and detail of celestial objects. They often appear as 
brilliant dots of light, showcasing their brightness and colors, making deep 
sky images truly captivating. However, due to the limitations of observing 
conditions, the stars in these images may appear larger and over-exposed. To 
combat this, astronomers use advanced image processing techniques to separately
process the stars and control their size and brightness in the final image. 

This part of the documentation is then dedicated to everything related to the 
stars processing.

.. toctree::
   :hidden:

   stars/starnet
   stars/star-recomp
   stars/unclipped
   stars/resynthesis

