Scripts
=======

Siril has a command line in its graphical user interface and an ability to run 
scripts that are a list of commands, either from the graphical user interface 
or from the command line interface. In general, commands that modify a single 
image work on the currently loaded image, so the use of the :ref:`load <load>` 
command is required in scripts, and commands that work on a sequence of images 
take the name of the sequence as argument. If files are not named in a way that
Siril detects as a sequence, the command :ref:`convert <convert>` will help.

.. tip::
   The :kbd:`Space` character is the delimiter between arguments. If you need 
   to have spaces inside the arguments, you can use the quote or double quote, 
   just like in a shell.

Commands can be typed in the command line at the bottom of Siril's main window.
Another way is to put commands in a file and execute it as a script. To execute
the file from the GUI, add it to the configured script directories or from the 
GUI, use the ``@`` token of the command line like so:

.. code-block:: text

   @file_name

Some commands (:ref:`preprocess <preprocess>`, :ref:`stack <stack>`, and all 
save commands) can use file names containing variables coming from the FITS 
header. The format of the expression is explained in details 
:ref:`here <Pathparsing:Path parsing>` and can be tested using the 
:ref:`parse <parse>` command.

Using scripts
*************
There are three ways to run a script:

* from the graphical user interface, using the ``@`` keyword on the command 
  line, followed by the script name in the current working directory,
* from the graphical user interface, using the :guilabel:`Scripts` menu,

  .. figure:: ./_images/scripts/Scripts_menu.jpg
     :alt: dialog
     :class: with-shadow

* from the command line interface (:program:`siril-cli` executable), using 
  argument ``-s`` followed by the script's path (see the man page for more 
  info).
  
The scripts menu only appears if some scripts have been found in the script 
search directories defined either by default or by the user in the preference 
menu.

Populates the list of scripts
*****************************

By default, when Siril is installed, a number of scripts are automatically 
installed. These built-in scripts, the official ones, are developed by the 
development team and are guaranteed to work: they are meant to cover specific 
use cases. For different reasons, it is possible that the :guilabel:`Scripts`
menu is not visible. This means that the scripts have not been found by Siril.

For different reasons, it is possible that the :guilabel:`Scripts` menu is not 
visible. This means that the scripts have not been found. If this is the case,
please use the following procedure.

* Click on the :guilabel:`Burger` icon then on :guilabel:`Preferences`.
* Click on the :guilabel:`Scripts` section.
* Delete all the lines in the field :guilabel:`Script Storage Directories`
  as shown in the illustration below.
* Click on :guilabel:`Apply`.
* Close and restart Siril.

.. figure:: ./_images/preferences/pref_8.png
   :alt: dialog
   :class: with-shadow
   :width: 100%

   Script page of preferences. The script are loaded from the paths listed in
   the :guilabel:`Script Storage Directories`.

Built-in scripts
****************

* **Mono_Preprocessing.ssf**: script for monochrome DSLR or Astro camera 
  preprocessing, uses biases, flats and darks, registers and stacks the images.
  To use it: put your files (RAW or FITs) in the folders named ``lights``, 
  ``darks``, ``flats`` and ``biases`` (in the Siril default working folder), 
  then run the script.
* **OSC_Preprocessing.ssf**: same script as above but for One-Shot Color (OSC) 
  DSLR or Astro camera. To use it: put your files (RAW or FITS) in the folders 
  named ``lights``, ``darks``, ``flats`` and ``biases`` (in the Siril default 
  working folder), then run the script.
* **OSC_Extract_Ha.ssf**: script for OSC DSLR or astro camera preprocessing, 
  for use with Ha filter or dual-band filter. This script extracts the Ha layer
  of the color image. To use it: put your files (RAW or FITs) in the folders 
  named ``lights``, ``darks``, ``flats`` and ``biases`` (in the Siril default 
  working folder), then run the script. 
* **OSC_Extract_HaOIII.ssf**: same script as above, but extracts Ha and OIII 
  layers of the color image. To use it: put your files (RAW or FITs) in the 
  folders named ``lights``, ``darks``, ``flats`` and ``biases`` (in the Siril 
  default working folder), then run the script. You can also use the menu 
  :guilabel:`Image Processing` then :guilabel:`RGB compositing` and put Ha 
  result in Red channel and OIII result in Green and Blue layers to get an HOO 
  image.
* **RGB_Composition.ssf**: This script added in version 1.2 registers
  monochrome images with a global registration, reframes them to their common
  area, and takes the first three images to create a color image. The input
  images should be put alone in a directory and named ``R.fit`` (or with the
  configured extension), ``G.fit`` and ``B.fit``. The result will be named
  ``rgb.fit``. Make sure you remove the ``process`` directory between each run.


.. rubric:: Language of scripts

At the beginning of the scripts, and thanks to the contribution of a user, the 
scripts existed in two versions (English, and French). When Siril 1.2.0 was 
released, it was decided to keep only the English scripts for simplicity of 
maintenance. We encourage users to distribute translations of the official 
scripts to their respective communities if they deem it necessary.


Writing your own
****************
.. |nonscriptable| image:: ./_images/icons/nonscriptable.svg
               :alt: Non scriptable
               
Writing a script is not difficult. It is a succession of calls to commands 
that will be executed sequentially. Each command must be executed without 
returning an error, otherwise the script stops. It is therefore strongly 
recommended to use the list of :ref:`commands <Commands:Commands>` to know the 
syntax and the number of parameters needed. Also, some commands are not 
scriptable and are indicated with the |nonscriptable| icon. It can also be 
useful to test each script line in the Siril command line. 

Each new script created in this way can be placed in a special folder, created 
by the user. This folder must however be indicated to Siril, in the 
:ref:`preferences <Preferences/preferences_gui:Scripts>` of the software.
